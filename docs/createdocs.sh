#! /bin/sh


#find . -name "*.sol" -not -path "*mocks*" -not -path "*Migrations*" -not -path "*node_modules*" -not -path "*examples*" -print | xargs surya inheritance > ./docs/inheritance.dot.txt 
#find . -name "*.sol" -not -path "*mocks*" -not -path "*Migrations*" -not -path "*node_modules*" -not -path "*examples*" -print | xargs surya inheritance | dot -Tpng > ./docs/inheritance.png
find . -name "*.sol" -not -path "*mocks*" -not -path "*Migrations*" -not -path "*node_modules*" -not -path "*examples*" -print | xargs surya mdreport ./docs/contracts.md
cp ./docs/contracts.md ../docusaurus/docs/architecture

find . -name "*.sol" -not -path "*mocks*" -not -path "*Migrations*" -not -path "*node_modules*" -not -path "*examples*" -print | xargs surya describe | aha --title "SmartRaise Contracts" > ./docs/describe.html

# dot -Tpng ./docs/inheritance.dot.txt > ./docs/inheritance-manual.png


solidity-docgen ./ ./contracts ../docusaurus/tmp



