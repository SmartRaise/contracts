## Sūrya's Description Report

### Files Description Table


|  File Name  |  SHA-1 Hash  |
|-------------|--------------|
| ./contracts/Smartraise.sol | f49f99847674d5f790f99b5e73def2fee0cfbc37 |
| ./contracts/payout/PayoutApproval.sol | 893346362f733957fd1bea5f2338ec7978d829ed |
| ./contracts/payout/Refund.sol | 1cc3f09fee1fafe5de56601dd644870467d3f9dc |
| ./contracts/discovery/discovery.sol | 5cfc1e55e06042e379b54885500c3649c33ab042 |
| ./contracts/rbac/Roles.sol | 18c191b888f8e43a11da92bc94df94b0da81feb8 |
| ./contracts/rbac/RBAC.sol | b7f9259c3911927d40ba018511d48eca4621ad0b |
| ./contracts/rbac/Auditor.sol | b000729caf956656a23916c04c91a93e82e7f7c4 |
| ./contracts/rbac/RbacSmartRaise.sol | 60fd202d6829a926466ef353581a4af3bcd63bbe |
| ./contracts/supporters/FundingStatus.sol | b96e300b9e1fa0af087c3cc99a2b085bb44281cb |
| ./contracts/supporters/SupportableStatistics.sol | b646b632d6c797975230b98cdf6479ad3eeb2876 |
| ./contracts/supporters/Payin.sol | 8c2c9e49b2a881d89e4e499870006dd66dc9bf4c |
| ./contracts/supporters/Supportable.sol | ee46ee496d99f41393a7ae52b0c9eb7187a041df |
| ./contracts/project/StateMachine.sol | 7d1eccc0711e981638776ac93364993e85e026cf |
| ./contracts/project/Content.sol | bb3aba99d4b3204e170835cf8725cecbccbc058b |
| ./contracts/project/ProjectInfo.sol | 906a9dfd006f0ad1c255c21b799ac088eb4165bb |
| ./contracts/project/PhaseSaver.sol | 7b4ef52b33f7c756f3f1a22f96e04582f492ac5d |
| ./contracts/project/Phases.sol | d389aac95db08bc861d21b855ddbe6ae7fa899c7 |
| ./contracts/project/ProjectStatus.sol | fac214d17dc03af60a08066d6094f900c958594b |
| ./contracts/math/SafeMath.sol | 7d42b26d5037c67d896fd7c3e453090c23d2971e |
| ./contracts/math/Math.sol | efb263fb6bfee9c7bfdfcdf9b5b390ed46947c0a |


### Contracts Description Table


|  Contract  |         Type        |       Bases      |                  |                 |
|:----------:|:-------------------:|:----------------:|:----------------:|:---------------:|
|     └      |  **Function Name**  |  **Visibility**  |  **Mutability**  |  **Modifiers**  |
||||||
| **Smartraise** | Implementation | SupportableStatistics, Payinable, Refundable, PayoutApproval |||
||||||
| **PayoutApproval** | Implementation | ProjectInfo, Supportable, Auditor |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
| └ | approvePayoutLimit | Public ❗️ | 🛑  | onlyRole onlyFundingStatus |
| └ | leftToPayoutLimit | Public ❗️ |   | |
| └ | payout | Public ❗️ | 🛑  | onlyRole |
||||||
| **Refundable** | Implementation | Supportable, StateMachine |||
| └ | preRefundProjectBalance | Public ❗️ |   | |
| └ | calculateRefundEntitlement | Public ❗️ |   | |
| └ | withdrawRefund | Public ❗️ | 🛑  | onlyFundingStatus |
| └ | bookRefund | Private 🔐 | 🛑  | |
||||||
| **Discovery** | Implementation |  |||
||||||
| **Roles** | Library |  |||
| └ | add | Internal 🔒 | 🛑  | |
| └ | remove | Internal 🔒 | 🛑  | |
| └ | check | Internal 🔒 |   | |
| └ | has | Internal 🔒 |   | |
||||||
| **RBAC** | Implementation |  |||
| └ | checkRole | Public ❗️ |   | |
| └ | hasRole | Public ❗️ |   | |
| └ | addRole | Internal 🔒 | 🛑  | |
| └ | removeRole | Internal 🔒 | 🛑  | |
||||||
| **Auditor** | Implementation | StateMachine |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
| └ | addAuditor | Public ❗️ | 🛑  | onlyRole onlyProjectStatus |
| └ | removeAuditor | Public ❗️ | 🛑  | onlyRole |
||||||
| **RbacSmartRaise** | Implementation | RBAC |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
| └ | addRoleOverride | Public ❗️ | 🛑  | onlyRole |
| └ | removeRoleOverride | Public ❗️ | 🛑  | onlyRole |
||||||
| **FundingStatus** | Implementation |  |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
||||||
| **SupportableStatistics** | Implementation | Supportable |||
| └ | getFundingTable | Public ❗️ |   | |
| └ | supporterStats | External ❗️ |   | |
||||||
| **Payinable** | Implementation | Supportable, StateMachine |||
| └ | addFunds | Public ❗️ |  💵 | onlyFundingStatus |
| └ | bookPayin | Internal 🔒 | 🛑  | |
||||||
| **Supportable** | Implementation |  |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
| └ | isSupporter | Public ❗️ |   | |
| └ | countSupporters | Public ❗️ |   | |
| └ | totalCollected | Public ❗️ |   | |
| └ | getSupporterAmount | Public ❗️ |   | |
| └ | getSupporterRefunded | Public ❗️ |   | |
||||||
| **StateMachine** | Implementation | FundingStatus, RbacSmartRaise, PhaseSaver |||
| └ | setStatusOverride | Public ❗️ | 🛑  | onlyRole |
| └ | setCollecting | Public ❗️ | 🛑  | onlyRole |
| └ | setHold | Public ❗️ | 🛑  | onlyRole |
| └ | setCanceled | Public ❗️ | 🛑  | onlyRole |
||||||
| **Content** | Implementation |  |||
||||||
| **ProjectInfo** | Implementation |  |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
||||||
| **PhaseSaver** | Implementation | Phases |||
| └ | setPhases | External ❗️ | 🛑  | onlyProjectStatus |
| └ | verifyAllPhases | External ❗️ | 🛑  | |
| └ | verifyPhaseValid | Private 🔐 |   | |
| └ | saveOrVerifyAllPhases | Private 🔐 | 🛑  | |
| └ | privateSavePhase | Private 🔐 | 🛑  | |
||||||
| **Phases** | Implementation | ProjectStatus |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
| └ | countPhases | Public ❗️ |   | |
| └ | getPhase | Public ❗️ |   | |
| └ | totalGoal | Public ❗️ |   | |
| └ | unlockedFunding | Public ❗️ |   | |
||||||
| **ProjectStatus** | Implementation |  |||
| └ | \<Constructor\> | Public ❗️ | 🛑  | |
||||||
| **SafeMath** | Library |  |||
| └ | mul | Internal 🔒 |   | |
| └ | div | Internal 🔒 |   | |
| └ | sub | Internal 🔒 |   | |
| └ | add | Internal 🔒 |   | |
||||||
| **Math** | Library |  |||
| └ | max64 | Internal 🔒 |   | |
| └ | min64 | Internal 🔒 |   | |
| └ | max256 | Internal 🔒 |   | |
| └ | min256 | Internal 🔒 |   | |


### Legend

|  Symbol  |  Meaning  |
|:--------:|-----------|
|    🛑    | Function can modify state |
|    💵    | Function is payable |
