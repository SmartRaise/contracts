pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

contract AuditorNote {

  struct AuditorNoteStruct {
    string note;
    address uploader;
    uint uploadDate;
    bytes32 status;
  }

  event AuditorNoteCreated(address indexed operator, uint indexed noteId);
  event AuditorNoteStatusChanged(address indexed operator, uint indexed noteId, bytes32 indexed newStatus);

  AuditorNoteStruct[] public auditorNotes;

   function addAuditorNote(string memory note) public{
    AuditorNoteStruct memory newNote = AuditorNoteStruct(note, msg.sender, block.timestamp, NOTE_PUBLISHED);
  
    auditorNotes.push(newNote);

    emit AuditorNoteCreated(msg.sender, countAuditorNotes()-1);
  }

   function getAuditorNote(uint _noteId) public view returns(AuditorNoteStruct memory){
      return auditorNotes[_noteId];
  }

   function countAuditorNotes() public view returns (uint) {
      return auditorNotes.length; 
  }

    bytes32 constant public NOTE_PUBLISHED = "PUBLISHED";
    bytes32 constant public NOTE_RETRACTED = "RETRACTED";

    function isValidNoteStatus(bytes32 _noteStatus) private pure returns (bool) {
        return (_noteStatus == NOTE_PUBLISHED || 
                _noteStatus == NOTE_RETRACTED);
    }

// TODO: Generalize in library
   function setNoteStatus(uint _noteId, bytes32 _newStatus) public {
      require(isValidNoteStatus(_newStatus), "New status must be valid");
      require(_noteId < countAuditorNotes(), "This note doesn't exist");
      auditorNotes[_noteId].status = _newStatus;

      emit AuditorNoteStatusChanged(msg.sender, _noteId, _newStatus);
  }
}

contract DescriptionContract {
  struct Description {
    string payload;
    address uploader;
    uint uploadDate;
  }

  Description[] public descriptions;
  uint public currentDescription;

  event DescriptionSet(address indexed operator, uint indexed descriptionId);

// TODO: 
  function setDescription(string memory payload) public{
    Description memory newPayload = Description(payload, msg.sender, block.timestamp);
  
    descriptions.push(newPayload);

    emit DescriptionSet(msg.sender, countDescriptions()-1);
  }

  // This is normally used to read current contract information
   function getDescription() public view returns(Description memory){
      return descriptions[countDescriptions()-1];
  }

  // This can be used to read history
   function getDescriptionById(uint _descriptionId) public view returns(Description memory){
      return descriptions[_descriptionId];
  }

   function countDescriptions() public view returns (uint) {
      return descriptions.length; 
  }

  // upload content
  // getcontent

}

library Math {
  function max64(uint64 a, uint64 b) internal pure returns (uint64) {
    return a >= b ? a : b;
  }

  function min64(uint64 a, uint64 b) internal pure returns (uint64) {
    return a < b ? a : b;
  }

  function max256(uint256 a, uint256 b) internal pure returns (uint256) {
    return a >= b ? a : b;
  }

  function min256(uint256 a, uint256 b) internal pure returns (uint256) {
    return a < b ? a : b;
  }
}

library SafeMath {
  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
    // Gas optimization: this is cheaper than asserting 'a' not being zero, but the
    // benefit is lost if 'b' is also tested.
    // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
    if (a == 0) {
      return 0;
    }

    c = a * b;
    assert(c / a == b);
    return c;
  }

  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    // uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return a / b;
  }

  /**
  * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256 c) {
    c = a + b;
    assert(c >= a);
    return c;
  }
}

contract ProjectInfo {
  /*************************/
  /* Project               */
  /*************************/
  uint16 public projectId;

  //modifier guardSender(address mbSender) {require(msg.sender == mbSender); _;}
  //function requireProjectStatus(eStatusProject mbState) private view {require(statusProject == mbState);}

  // Initialize with an empty project
  constructor() public {
    projectId = 333; // TODO: No way to make this unique, except via discovery service
  }
}

contract ProjectStatus {
  // TODO: Make it Bytes32-based
  enum eStatusProject {Draft, Collecting, Canceled, Hold, Funded}

  eStatusProject public statusProject;

  modifier onlyProjectStatus(eStatusProject _state) {
    require(
      statusProject == _state,
      "onlyProjectStatus check failed.");
    _;
  }

  constructor() public {
    statusProject = eStatusProject.Draft;
  }
}

contract RBAC {
  using Roles for Roles.Role;

  mapping(string => Roles.Role) private roles;

  event RoleAdded(address indexed operator, string role);
  event RoleRemoved(address indexed operator, string role);

  /**
   * @dev reverts if addr does not have role
   * @param _operator address
   * @param _role the name of the role
   * // reverts
   */
  function checkRole(address _operator, string memory _role) public view {
    roles[_role].check(_operator);
  }

  /**
   * @dev determine if addr has role
   * @param _operator address
   * @param _role the name of the role
   * @return bool
   */
  function hasRole(address _operator, string memory _role)
    public
    view
    returns (bool)
  {
    return roles[_role].has(_operator);
  }

  /**
   * @dev add a role to an address
   * @param _operator address
   * @param _role the name of the role
   */
  function addRole(address _operator, string memory _role) internal {
    roles[_role].add(_operator);
    emit RoleAdded(_operator, _role);
  }

  /**
   * @dev remove a role from an address
   * @param _operator address
   * @param _role the name of the role
   */
  function removeRole(address _operator, string memory _role) internal {
    roles[_role].remove(_operator);
    emit RoleRemoved(_operator, _role);
  }

  /**
   * @dev modifier to scope access to a single role (uses msg.sender as addr)
   * @param _role the name of the role
   * // reverts
   */
  modifier onlyRole(string memory _role) {
    checkRole(msg.sender, _role);
    _;
  }

  /**
   * @dev modifier to scope access to a set of roles (uses msg.sender as addr)
   * @param _roles the names of the roles to scope access to
   * // reverts
   *
   * @TODO - when solidity supports dynamic arrays as arguments to modifiers, provide this
   *  see: https://github.com/ethereum/solidity/issues/2467
   */
  // modifier onlyRoles(string[] _roles) {
  //     bool hasAnyRole = false;
  //     for (uint8 i = 0; i < _roles.length; i++) {
  //         if (hasRole(msg.sender, _roles[i])) {
  //             hasAnyRole = true;
  //             break;
  //         }
  //     }

  //     require(hasAnyRole);

  //     _;
  // }

}

contract RbacSmartRaise is RBAC {
  // using Roles for Roles.Role;

  /**
     * A constant role name for indicating admins.
     */
  string public constant ROLE_PM = "pm";
  string public constant ROLE_PLATFORM = "platform";
  string public constant ROLE_AUDITOR = "auditor";

  modifier eitherRole3(
    string memory _role1,
    string memory _role2,
    string memory _role3
  ) {
    require(
      hasRole(msg.sender, _role1) || hasRole(msg.sender, _role2) || hasRole(
        msg.sender,
        _role3
      )
    );
    _;
  }

  modifier eitherRole2(string memory _role1, string memory _role2) {
    require(
      hasRole(msg.sender, _role1) || hasRole(msg.sender, _role2),
      "hasRole check failed.");
    _;
  }

  /**
     * @dev constructor. Sets msg.sender as platform and project manager by default
     */
  constructor() public {
    // addRole(msg.sender, ROLE_PLATFORM);
    addRole(msg.sender, ROLE_PM);
  }

  /**
     * @dev add a role to an address
     * @param addr address
     * @param roleName the name of the role
     */
  function addRoleOverride(address addr, string memory roleName)
    public
    onlyRole(ROLE_PLATFORM)
  {
    addRole(addr, roleName);
  }

  /**
     * @dev remove a role from an address
     * @param addr address
     * @param roleName the name of the role
     */
  function removeRoleOverride(address addr, string memory roleName)
    public
    onlyRole(ROLE_PLATFORM)
  {
    removeRole(addr, roleName);
  }
}

contract Phases is ProjectStatus, RbacSmartRaise {
  uint public constant MAXPHASES = 5;
  uint public constant MINGOAL = 25;

  Phase[] public phases;

  struct Phase {
    bytes32 label;
    uint goal;
    uint dueDate;
    uint accumulatedGoal;
  }

  uint public activePhase;

  constructor() public {
    activePhase = 0;
  }

  event SetActivePhase(address indexed operator, uint activePhase);

  function setActivePhase(uint _phaseNr)
    public
    onlyRole(ROLE_AUDITOR)
  {
    activePhase = _phaseNr;

    emit SetActivePhase(msg.sender, _phaseNr);
  }

  function countPhases() public view returns (uint) {
    return phases.length;
  }

  function getPhase(uint _phNr) public view returns (Phase memory) {
    return phases[_phNr];
  }

  function totalGoal() public view returns (uint) {
    if (countPhases() > 0) {
      return getPhase(phases.length - 1).accumulatedGoal;
    } else return 0;
  }

  /// Total unlocked funding based on currently active phase. Actual payout limit is
  /// set by auditor and cannot be higher than the unlocked funding.
  function unlockedFunding() public view returns (uint) {
    return getPhase(activePhase).accumulatedGoal;
  }
}

contract PhaseSaver is Phases {
  /*************************/
  /* Edit                 */
  /*************************/

  /// (Re-)Sets phases. Only works in Draft, deletes existing entries
  function setPhases(
    bytes32[] calldata _labels, 
    uint[] calldata _goals, 
    uint[] calldata _dueDates
  )
    external
    onlyProjectStatus(eStatusProject.Draft)
  { 
    phases.length = 0; // We reset the array. We don't use delete to save gas.
    saveOrVerifyAllPhases(_labels, _goals, _dueDates, true);
  }

  /// @dev This is external so that we can pre-check for free before we even deploy
  function verifyAllPhases(
    bytes32[] calldata _labels, 
    uint[] calldata _goals,
    uint[] calldata _dueDates
  ) external {
    saveOrVerifyAllPhases(_labels, _goals, _dueDates, false);
  }

  function verifyPhaseValid(
    Phase memory _preceedingPhase,
    Phase memory _currentPhase
  ) private pure returns (bool) {
    require(_currentPhase.goal >= MINGOAL, "Phase goal below minimum.");
    require(
      _currentPhase.dueDate >= _preceedingPhase.dueDate,
      "Timeout dates must be increasing."
    );
    return true;
  }

  // ** !Warning: Never expose directly, no security checks! **//
  function saveOrVerifyAllPhases(
    bytes32[] memory _labels,
    uint[] memory _goals,
    uint[] memory _dueDates,
    bool _save
  ) private {
    // Check data form validity
    uint cntGoals = _goals.length;
    require(cntGoals == _dueDates.length, "Data malformed");
    require(countPhases() + cntGoals <= MAXPHASES, "Too many phases.");

    uint16 i;
    Phase memory preceedingPhase = Phase("", 0, 0, 0);
    Phase memory currentPhase;

    // Loop to verify and save
    for (i = 0; i < cntGoals; i++) {
      // Construct structs
      currentPhase.label = _labels[i];
      currentPhase.goal = _goals[i];
      currentPhase.accumulatedGoal = preceedingPhase.accumulatedGoal + currentPhase.goal;
      currentPhase.dueDate = _dueDates[i];

      // Verify Phase (triggers requires in it)
      verifyPhaseValid(preceedingPhase, currentPhase);

      // Save it. Don't worry, if one of next phase checks
      // fails, all changes are discarded.
      if (_save == true) privateSavePhase(currentPhase);

      // Move one seat
      preceedingPhase = currentPhase;
    }
  }

  // ** !Warning: Never expose, no security checks! **//
  function privateSavePhase(Phase memory _phase) private {
    phases.push(_phase);
  }
}

library Roles {
  struct Role {
    mapping(address => bool) bearer;
  }

  /**
   * @dev give an address access to this role
   */
  function add(Role storage role, address addr) internal {
    role.bearer[addr] = true;
  }

  /**
   * @dev remove an address' access to this role
   */
  function remove(Role storage role, address addr) internal {
    role.bearer[addr] = false;
  }

  /**
   * @dev check if an address has this role
   * // reverts
   */
  function check(Role storage role, address addr) internal view {
    require(
      has(role, addr),
      "Role check failed.");
  }

  /**
   * @dev check if an address has this role
   * @return bool
   */
  function has(Role storage role, address addr) internal view returns (bool) {
    return role.bearer[addr];
  }
}

contract FundingStatus {
  // Status of Funding
  enum eStatusFunding {None, Open, Hold, Refund, Completed}

  eStatusFunding public statusFunding;

  modifier onlyFundingStatus(eStatusFunding _state) {
    require(statusFunding == _state, "Funding status must be appropriate");
    _;
  }

  constructor() public {
    statusFunding = eStatusFunding.None;
  }
}

contract StateMachine is FundingStatus, RbacSmartRaise, PhaseSaver {

  /// Platform override: Platform Role can set whatever they want
  function setStatusOverride(
    eStatusProject _newProjectStatus,
    eStatusFunding _newFundingStatus
  ) public onlyRole(ROLE_PLATFORM) {
    statusProject = _newProjectStatus;
    statusFunding = _newFundingStatus;
  }

  /// Draft / Hold -> Collecting    
  function setCollecting() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Draft || statusProject == eStatusProject.Hold,
      "Project must be on Hold or Draft to perform setCollecting()"
    );
    require(
      Phases.countPhases() > 0,
     "Project must have at least one Phase set to perform setCollecting()");
    // require(auditor != 0);
    // TODO: Fundinggoal

    statusProject = eStatusProject.Collecting;
    statusFunding = eStatusFunding.Open; // TODO: Why did I comment this out earlier?
  }

  /// Collecting -> Hold, 
  function setHold() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Collecting, 
      "Project must be in Status Collecting to perform setHold()");
      
    statusProject = eStatusProject.Hold;
    statusFunding = eStatusFunding.Hold;
  }

// Todo: Maybe allow to set from other states too?
  function setFunded() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Collecting, 
      "Project must be in Status Collecting to perform setFunded()");
      
    statusProject = eStatusProject.Funded;
    statusFunding = eStatusFunding.Completed;
  }


  /// -> Cancel & Allow Refund, unless it has already ended
  function setCanceled() public onlyRole(ROLE_PM) {
    require(
      statusProject != eStatusProject.Funded, 
      "Project cannot be in Status Funded when performing setCanceled()");

    statusProject = eStatusProject.Canceled;
    statusFunding = eStatusFunding.Refund;

    //TODO: Should we reset payout stuff here?

  }
}

contract Auditor is StateMachine {
  constructor() public {}

  function addAuditor(address newAuditor)
    public
    onlyRole(ROLE_PM)
    onlyProjectStatus(eStatusProject.Draft)
  {
    addRole(newAuditor, ROLE_AUDITOR);
  }

  function removeAuditor(address newAuditor) public onlyRole(ROLE_PM) {
    removeRole(newAuditor, ROLE_AUDITOR);
  }

}

contract Supportable {
  struct Balances {
    address[] supporterAddresses; // This is be able to iterate
    mapping(address => uint) supporterAmounts; // This is to direct access data
    mapping(address => uint) supporterRefunded; // This is to direct access data
    uint totalCollected;
    uint totalRefunded;
    uint paidOutToProject;
  }

  Balances internal balances;

  constructor() public {
    balances.totalCollected = 0;
  }

  function isSupporter(address parAdd) public view returns (bool) {
    if (balances.supporterAmounts[parAdd] > 0) return true;
    else return false;
  }

  function countSupporters() public view returns (uint) {
    return balances.supporterAddresses.length;
  }

  function totalCollected() public view returns (uint) {
    return balances.totalCollected;
  }

  function getSupporterAmount(address supAddr) public view returns (uint) {
    return balances.supporterAmounts[supAddr];
  }

  function getSupporterRefunded(address supAddr) public view returns (uint) {
    return balances.supporterRefunded[supAddr];
  }

}

contract PayoutApproval is ProjectInfo, Supportable, Auditor {
  using SafeMath for uint256;
  using Math for uint256;

  uint public approvedPayoutLimit;

  event AuditorNewPayoutLimit(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  event Payout(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  constructor() public {
    approvedPayoutLimit = 0;
    balances.paidOutToProject = 0;
  }

  /// Sets the payout limit. The limit cannot be set lower than what has already been paid out.
  function approvePayoutLimit(uint _newPayoutLimit)
    public
    onlyRole(ROLE_AUDITOR)
    onlyFundingStatus(eStatusFunding.Completed)
  {
    require(
      _newPayoutLimit <= unlockedFunding(),
      "New Payout Limit must be =< unlocked Funding"
    );

    approvedPayoutLimit = Math.max256(
      _newPayoutLimit,
      balances.paidOutToProject
    );

    emit AuditorNewPayoutLimit(projectId, msg.sender, approvedPayoutLimit);
  }

  function leftToPayoutLimit() public view returns (uint) {
    if (balances.paidOutToProject >= approvedPayoutLimit) {
      return 0;
    } else {
      return approvedPayoutLimit.sub(balances.paidOutToProject);
    }
  }

  function payout(uint _amount) public onlyRole(ROLE_PM) {
    require(
      _amount <= leftToPayoutLimit(),
      "Payout amount must be =< lefToPayoutLimit"
    );
    balances.paidOutToProject = balances.paidOutToProject.add(_amount);
    address(msg.sender).transfer(approvedPayoutLimit);
    emit Payout(333, msg.sender, _amount);
  }
}

contract Refundable is Supportable, StateMachine {
  using SafeMath for uint256;

  event Refund(
    uint indexed projectId,
    address indexed supporter,
    uint indexed amount
  );

  function preRefundProjectBalance() public view returns (uint) {
    return balances.totalCollected.sub(balances.paidOutToProject);
  }

  function calculateRefundEntitlement(address _supporter)
    public
    view
    returns (uint)
  {
    return getSupporterAmount(_supporter).mul(preRefundProjectBalance()).div(
      balances.totalCollected
    ) - getSupporterRefunded(_supporter);
  }

  /// Pays out refund to the caller. No payout to other addresses possible for now.
  function withdrawRefund() public onlyFundingStatus(eStatusFunding.Refund) {
    uint refundValue = calculateRefundEntitlement(msg.sender);
    bookRefund(msg.sender, refundValue);
    address(msg.sender).transfer(refundValue);

    emit Refund(333, msg.sender, refundValue);
  }

  function bookRefund(address _supporter, uint _value) private {
    balances.supporterRefunded[_supporter] = balances.supporterRefunded[_supporter].add(
      _value
    );
    balances.totalRefunded = balances.totalRefunded.add(_value);
  }
}

contract Payinable is Supportable, StateMachine {
  using SafeMath for uint256;

  uint public constant MINPAYIN = 25; // Min Payin to avoid DOS

  event Payin(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  function () external payable {
    addFunds();
  } 

  function addFunds() public payable onlyFundingStatus(eStatusFunding.Open) {
    require(msg.value >= MINPAYIN, "Payin must be bigger than MINPAYIN"); // Minimum Payin to avoid DOS

    bookPayin(msg.sender, msg.value);

    //TODO: Remove projectIds
    emit Payin(333, msg.sender, msg.value);
  }

  function bookPayin(address _sender, uint _value) internal {
    balances.supporterAmounts[_sender] = balances.supporterAmounts[_sender] + _value;
    balances.totalCollected += _value;

    if (!isSupporter(_sender)) balances.supporterAddresses.push(_sender);
  }

}

contract SupportableStatistics is Supportable {
  /** Returns two arrays: Addresses of supporters, Amount they paid in */
  function getFundingTable()
    public
    view
    returns (address[] memory, uint[] memory)
  {
    uint[] memory fundingTable;

    for (uint i = 0; i < countSupporters(); i++) {
      fundingTable[i] = (getSupporterAmount(balances.supporterAddresses[i]));
    }

    return (balances.supporterAddresses, fundingTable);
  }

  /** Returns total collected and supporter table
  // TODO: REVIEW: It's only a convenience function, do we really need this? */
  function supporterStats()
    external
    view
    returns (uint, address[] memory, uint[] memory)
  {
    address[] memory addr;
    uint[] memory amount;

    (addr, amount) = getFundingTable();
    return (balances.totalCollected, addr, amount);
  }
}

contract Smartraise is SupportableStatistics, Payinable, Refundable, PayoutApproval, AuditorNote, DescriptionContract {

  constructor() public {
    setDescription("");
  }
}

