pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

contract AuditorNote {

  struct AuditorNoteStruct {
    string note;
    address uploader;
    uint uploadDate;
    bytes32 status;
  }

  AuditorNoteStruct[] public auditorNotes;

   function addAuditorNote(string memory note) public{
    AuditorNoteStruct memory newNote = AuditorNoteStruct(note, msg.sender, block.timestamp, NOTE_PUBLISHED);
  
    auditorNotes.push(newNote);

    emit AuditorNoteCreated(msg.sender, countAuditorNotes()-1);
  }

   function getAuditorNote(uint _noteId) public view returns(AuditorNoteStruct memory){
      return auditorNotes[_noteId];
  }

   function countAuditorNotes() public view returns (uint) {
      return auditorNotes.length; 
  }

    bytes32 constant public NOTE_PUBLISHED = "PUBLISHED";
    bytes32 constant public NOTE_RETRACTED = "RETRACTED";

    function isValidNoteStatus(bytes32 _noteStatus) private pure returns (bool) {
        return (_noteStatus == NOTE_PUBLISHED || 
                _noteStatus == NOTE_RETRACTED);
    }

    event AuditorNoteCreated(address indexed operator, uint indexed noteId);
    event AuditorNoteStatusChanged(address indexed operator, uint indexed noteId, bytes32 indexed newStatus);

// TODO: Generalize in library
   function setNoteStatus(uint _noteId, bytes32 _newStatus) public {
      require(isValidNoteStatus(_newStatus), "New status must be valid");
      require(_noteId < countAuditorNotes(), "This note doesn't exist");
      auditorNotes[_noteId].status = _newStatus;

      emit AuditorNoteStatusChanged(msg.sender, _noteId, _newStatus);
  }
}

