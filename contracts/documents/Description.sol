pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

// We save the old versions to have an audit log
// Always the most recent version is the version being displayed
// Never iterate over the array, it could be flooded by a malicious project manager
contract DescriptionContract {
  struct Description {
    string payload;
    address uploader;
    uint uploadDate;
  }

  Description[] public descriptions;
  uint public currentDescription;

  event DescriptionSet(address indexed operator, uint indexed descriptionId);

// TODO: 
  function setDescription(string memory payload) public{
    Description memory newPayload = Description(payload, msg.sender, block.timestamp);
  
    descriptions.push(newPayload);

    emit DescriptionSet(msg.sender, countDescriptions()-1);
  }

  // This is normally used to read current contract information
   function getDescription() public view returns(Description memory){
      return descriptions[countDescriptions()-1];
  }

  // This can be used to read history
   function getDescriptionById(uint _descriptionId) public view returns(Description memory){
      return descriptions[_descriptionId];
  }

   function countDescriptions() public view returns (uint) {
      return descriptions.length; 
  }

  // upload content
  // getcontent

}
