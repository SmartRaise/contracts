pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

/** The directory saves all active campaings. Current implementation is not ready for prime-time,
no security, no checks, not all functions implemented, can be easily flooded */
contract Directory {
  struct Project {
    address projectContract;
    bytes32 status;
  }

  address[] public campaign;

  function addCampaign(address _campaign) public {
    campaign.push(_campaign);
  }
  
  function countCampaigns() public view returns (uint) {
    return campaign.length;
  }

  function getCampaign(uint _campaignNr) public view returns (address) {
    return campaign[_campaignNr];
  }

  function getCampaigns() public view returns (address[] memory)
  {
    return campaign;
  }


  //AddProject
  //StatusUpdateProject
  //getValidcampaign

}
