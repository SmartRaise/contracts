pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

// TODO: Events
// TODO: Payment

// pragma experimental ABIEncoderV2;

import "./supporters/SupportableStatistics.sol";
import "./supporters/Payin.sol";
import "./payout/Refund.sol";
import "./payout/PayoutApproval.sol";
import "./documents/AuditorNote.sol";
import "./documents/Description.sol";

contract Smartraise is SupportableStatistics, Payinable, Refundable, PayoutApproval, AuditorNote, DescriptionContract {

  constructor() public {
    setDescription("");
  }
}
