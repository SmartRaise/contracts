pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "./Supportable.sol";
import "../project/StateMachine.sol";
import "../math/SafeMath.sol";

contract Payinable is Supportable, StateMachine {
  using SafeMath for uint256;

  uint public constant MINPAYIN = 25; // Min Payin to avoid DOS

  event Payin(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  function () external payable {
    addFunds();
  } 

  function addFunds() public payable onlyFundingStatus(eStatusFunding.Open) {
    require(msg.value >= MINPAYIN, "Payin must be bigger than MINPAYIN"); // Minimum Payin to avoid DOS

    bookPayin(msg.sender, msg.value);

    //TODO: Remove projectIds
    emit Payin(333, msg.sender, msg.value);
  }

  function bookPayin(address _sender, uint _value) internal {
    balances.supporterAmounts[_sender] = balances.supporterAmounts[_sender] + _value;
    balances.totalCollected += _value;

    if (!isSupporter(_sender)) balances.supporterAddresses.push(_sender);
  }

}
