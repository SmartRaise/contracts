pragma solidity ^0.5.1;

contract Supportable {
  struct Balances {
    address[] supporterAddresses; // This is be able to iterate
    mapping(address => uint) supporterAmounts; // This is to direct access data
    mapping(address => uint) supporterRefunded; // This is to direct access data
    uint totalCollected;
    uint totalRefunded;
    uint paidOutToProject;
  }

  Balances internal balances;

  constructor() public {
    balances.totalCollected = 0;
  }

  function isSupporter(address parAdd) public view returns (bool) {
    if (balances.supporterAmounts[parAdd] > 0) return true;
    else return false;
  }

  function countSupporters() public view returns (uint) {
    return balances.supporterAddresses.length;
  }

  function totalCollected() public view returns (uint) {
    return balances.totalCollected;
  }

  function getSupporterAmount(address supAddr) public view returns (uint) {
    return balances.supporterAmounts[supAddr];
  }

  function getSupporterRefunded(address supAddr) public view returns (uint) {
    return balances.supporterRefunded[supAddr];
  }

}
