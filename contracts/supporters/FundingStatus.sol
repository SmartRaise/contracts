pragma solidity ^0.5.1;

contract FundingStatus {
  // Status of Funding
  enum eStatusFunding {None, Open, Hold, Refund, Completed}

  eStatusFunding public statusFunding;

  modifier onlyFundingStatus(eStatusFunding _state) {
    require(statusFunding == _state, "Funding status must be appropriate");
    _;
  }

  constructor() public {
    statusFunding = eStatusFunding.None;
  }
}
