pragma solidity ^0.5.1;

import "./Supportable.sol";

contract SupportableStatistics is Supportable {
  /** Returns two arrays: Addresses of supporters, Amount they paid in */
  function getFundingTable()
    public
    view
    returns (address[] memory, uint[] memory)
  {
    uint[] memory fundingTable;

    for (uint i = 0; i < countSupporters(); i++) {
      fundingTable[i] = (getSupporterAmount(balances.supporterAddresses[i]));
    }

    return (balances.supporterAddresses, fundingTable);
  }

  /** Returns total collected and supporter table
  // TODO: REVIEW: It's only a convenience function, do we really need this? */
  function supporterStats()
    external
    view
    returns (uint, address[] memory, uint[] memory)
  {
    address[] memory addr;
    uint[] memory amount;

    (addr, amount) = getFundingTable();
    return (balances.totalCollected, addr, amount);
  }
}
