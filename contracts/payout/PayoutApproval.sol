pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "../rbac/Auditor.sol";
import "../supporters/Supportable.sol";

import "../math/SafeMath.sol";
import "../math/Math.sol";
import "../project/ProjectInfo.sol";

/**  */
contract PayoutApproval is ProjectInfo, Supportable, Auditor {
  using SafeMath for uint256;
  using Math for uint256;

  uint public approvedPayoutLimit;

  event AuditorNewPayoutLimit(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  event Payout(
    uint indexed projectId,
    address indexed sender,
    uint indexed amount
  );

  constructor() public {
    approvedPayoutLimit = 0;
    balances.paidOutToProject = 0;
  }

  /// Sets the payout limit. The limit cannot be set lower than what has already been paid out.
  function approvePayoutLimit(uint _newPayoutLimit)
    public
    onlyRole(ROLE_AUDITOR)
    onlyFundingStatus(eStatusFunding.Completed)
  {
    require(
      _newPayoutLimit <= unlockedFunding(),
      "New Payout Limit must be =< unlocked Funding"
    );

    approvedPayoutLimit = Math.max256(
      _newPayoutLimit,
      balances.paidOutToProject
    );

    emit AuditorNewPayoutLimit(projectId, msg.sender, approvedPayoutLimit);
  }

  function leftToPayoutLimit() public view returns (uint) {
    if (balances.paidOutToProject >= approvedPayoutLimit) {
      return 0;
    } else {
      return approvedPayoutLimit.sub(balances.paidOutToProject);
    }
  }

  function payout(uint _amount) public onlyRole(ROLE_PM) {
    require(
      _amount <= leftToPayoutLimit(),
      "Payout amount must be =< lefToPayoutLimit"
    );
    balances.paidOutToProject = balances.paidOutToProject.add(_amount);
    address(msg.sender).transfer(approvedPayoutLimit);
    emit Payout(333, msg.sender, _amount);
  }
}
