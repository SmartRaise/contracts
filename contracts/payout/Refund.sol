pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "../supporters/Supportable.sol";
import "../math/SafeMath.sol";
import "../project/StateMachine.sol";

/** This contract manages refunds. Once refunds are triggerd, payins are prohibited.
Refunds are made proportionally to the users contribution. */
contract Refundable is Supportable, StateMachine {
  using SafeMath for uint256;

  event Refund(
    uint indexed projectId,
    address indexed supporter,
    uint indexed amount
  );

  function preRefundProjectBalance() public view returns (uint) {
    return balances.totalCollected.sub(balances.paidOutToProject);
  }

  function calculateRefundEntitlement(address _supporter)
    public
    view
    returns (uint)
  {
    return getSupporterAmount(_supporter).mul(preRefundProjectBalance()).div(
      balances.totalCollected
    ) - getSupporterRefunded(_supporter);
  }

  /// Pays out refund to the caller. No payout to other addresses possible for now.
  function withdrawRefund() public onlyFundingStatus(eStatusFunding.Refund) {
    uint refundValue = calculateRefundEntitlement(msg.sender);
    bookRefund(msg.sender, refundValue);
    address(msg.sender).transfer(refundValue);

    emit Refund(333, msg.sender, refundValue);
  }

  function bookRefund(address _supporter, uint _value) private {
    balances.supporterRefunded[_supporter] = balances.supporterRefunded[_supporter].add(
      _value
    );
    balances.totalRefunded = balances.totalRefunded.add(_value);
  }
}
