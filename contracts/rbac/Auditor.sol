pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "../project/StateMachine.sol";

// import "../project/ProjectProjectStatus.sol";
import "../rbac/RbacSmartRaise.sol";

contract Auditor is StateMachine {
  constructor() public {}

  function addAuditor(address newAuditor)
    public
    onlyRole(ROLE_PM)
    onlyProjectStatus(eStatusProject.Draft)
  {
    addRole(newAuditor, ROLE_AUDITOR);
  }

  function removeAuditor(address newAuditor) public onlyRole(ROLE_PM) {
    removeRole(newAuditor, ROLE_AUDITOR);
  }

}
