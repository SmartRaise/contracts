pragma solidity ^0.5.1;

/**
 * @title Roles
 * @author Francisco Giordano (@frangio)
 * @dev Library for managing addresses assigned to a Role.
 * See RBAC.sol for example usage.
 */
library Roles {
  struct Role {
    mapping(address => bool) bearer;
  }

  /**
   * @dev give an address access to this role
   */
  function add(Role storage role, address addr) internal {
    role.bearer[addr] = true;
  }

  /**
   * @dev remove an address' access to this role
   */
  function remove(Role storage role, address addr) internal {
    role.bearer[addr] = false;
  }

  /**
   * @dev check if an address has this role
   * // reverts
   */
  function check(Role storage role, address addr) internal view {
    require(
      has(role, addr),
      "Role check failed.");
  }

  /**
   * @dev check if an address has this role
   * @return bool
   */
  function has(Role storage role, address addr) internal view returns (bool) {
    return role.bearer[addr];
  }
}
