pragma solidity ^0.5.1;

import "../rbac/RBAC.sol";
// import "../rbac/Roles.sol";

contract RbacSmartRaise is RBAC {
  // using Roles for Roles.Role;

  /**
     * A constant role name for indicating admins.
     */
  string public constant ROLE_PM = "pm";
  string public constant ROLE_PLATFORM = "platform";
  string public constant ROLE_AUDITOR = "auditor";

  modifier eitherRole3(
    string memory _role1,
    string memory _role2,
    string memory _role3
  ) {
    require(
      hasRole(msg.sender, _role1) || hasRole(msg.sender, _role2) || hasRole(
        msg.sender,
        _role3
      )
    );
    _;
  }

  modifier eitherRole2(string memory _role1, string memory _role2) {
    require(
      hasRole(msg.sender, _role1) || hasRole(msg.sender, _role2),
      "hasRole check failed.");
    _;
  }

  /**
     * @dev constructor. Sets msg.sender as platform and project manager by default
     */
  constructor() public {
    // addRole(msg.sender, ROLE_PLATFORM);
    addRole(msg.sender, ROLE_PM);
  }

  /**
     * @dev add a role to an address
     * @param addr address
     * @param roleName the name of the role
     */
  function addRoleOverride(address addr, string memory roleName)
    public
    onlyRole(ROLE_PLATFORM)
  {
    addRole(addr, roleName);
  }

  /**
     * @dev remove a role from an address
     * @param addr address
     * @param roleName the name of the role
     */
  function removeRoleOverride(address addr, string memory roleName)
    public
    onlyRole(ROLE_PLATFORM)
  {
    removeRole(addr, roleName);
  }
}
