pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "./Phases.sol";

// TODO: This should be a library maybe...
contract PhaseSaver is Phases {
  /*************************/
  /* Edit                 */
  /*************************/

  /// (Re-)Sets phases. Only works in Draft, deletes existing entries
  function setPhases(
    bytes32[] calldata _labels, 
    uint[] calldata _goals, 
    uint[] calldata _dueDates
  )
    external
    onlyProjectStatus(eStatusProject.Draft)
  { 
    phases.length = 0; // We reset the array. We don't use delete to save gas.
    saveOrVerifyAllPhases(_labels, _goals, _dueDates, true);
  }

  /// @dev This is external so that we can pre-check for free before we even deploy
  function verifyAllPhases(
    bytes32[] calldata _labels, 
    uint[] calldata _goals,
    uint[] calldata _dueDates
  ) external {
    saveOrVerifyAllPhases(_labels, _goals, _dueDates, false);
  }

  function verifyPhaseValid(
    Phase memory _preceedingPhase,
    Phase memory _currentPhase
  ) private pure returns (bool) {
    require(_currentPhase.goal >= MINGOAL, "Phase goal below minimum.");
    require(
      _currentPhase.dueDate >= _preceedingPhase.dueDate,
      "Timeout dates must be increasing."
    );
    return true;
  }

  // ** !Warning: Never expose directly, no security checks! **//
  function saveOrVerifyAllPhases(
    bytes32[] memory _labels,
    uint[] memory _goals,
    uint[] memory _dueDates,
    bool _save
  ) private {
    // Check data form validity
    uint cntGoals = _goals.length;
    require(cntGoals == _dueDates.length, "Data malformed");
    require(countPhases() + cntGoals <= MAXPHASES, "Too many phases.");

    uint16 i;
    Phase memory preceedingPhase = Phase("", 0, 0, 0);
    Phase memory currentPhase;

    // Loop to verify and save
    for (i = 0; i < cntGoals; i++) {
      // Construct structs
      currentPhase.label = _labels[i];
      currentPhase.goal = _goals[i];
      currentPhase.accumulatedGoal = preceedingPhase.accumulatedGoal + currentPhase.goal;
      currentPhase.dueDate = _dueDates[i];

      // Verify Phase (triggers requires in it)
      verifyPhaseValid(preceedingPhase, currentPhase);

      // Save it. Don't worry, if one of next phase checks
      // fails, all changes are discarded.
      if (_save == true) privateSavePhase(currentPhase);

      // Move one seat
      preceedingPhase = currentPhase;
    }
  }

  // ** !Warning: Never expose, no security checks! **//
  function privateSavePhase(Phase memory _phase) private {
    phases.push(_phase);
  }
}
