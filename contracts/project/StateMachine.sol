pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "../rbac/RbacSmartRaise.sol";
import "./PhaseSaver.sol";
import "../supporters/FundingStatus.sol";
import "../project/StateMachine.sol";

/** The State Machine governs the transitions between project states. All status changes must happen here. */
contract StateMachine is FundingStatus, RbacSmartRaise, PhaseSaver {

  /// Platform override: Platform Role can set whatever they want
  function setStatusOverride(
    eStatusProject _newProjectStatus,
    eStatusFunding _newFundingStatus
  ) public onlyRole(ROLE_PLATFORM) {
    statusProject = _newProjectStatus;
    statusFunding = _newFundingStatus;
  }

  /// Draft / Hold -> Collecting    
  function setCollecting() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Draft || statusProject == eStatusProject.Hold,
      "Project must be on Hold or Draft to perform setCollecting()"
    );
    require(
      Phases.countPhases() > 0,
     "Project must have at least one Phase set to perform setCollecting()");
    // require(auditor != 0);
    // TODO: Fundinggoal

    statusProject = eStatusProject.Collecting;
    statusFunding = eStatusFunding.Open; // TODO: Why did I comment this out earlier?
  }

  /// Collecting -> Hold, 
  function setHold() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Collecting, 
      "Project must be in Status Collecting to perform setHold()");
      
    statusProject = eStatusProject.Hold;
    statusFunding = eStatusFunding.Hold;
  }

// Todo: Maybe allow to set from other states too?
  function setFunded() public onlyRole(ROLE_PM) {
    require(
      statusProject == eStatusProject.Collecting, 
      "Project must be in Status Collecting to perform setFunded()");
      
    statusProject = eStatusProject.Funded;
    statusFunding = eStatusFunding.Completed;
  }


  /// -> Cancel & Allow Refund, unless it has already ended
  function setCanceled() public onlyRole(ROLE_PM) {
    require(
      statusProject != eStatusProject.Funded, 
      "Project cannot be in Status Funded when performing setCanceled()");

    statusProject = eStatusProject.Canceled;
    statusFunding = eStatusFunding.Refund;

    //TODO: Should we reset payout stuff here?

  }
}


// rename Collecting -> collecting
// Ended -> Delivering
// Ended