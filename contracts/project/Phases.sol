pragma solidity ^0.5.1;
pragma experimental ABIEncoderV2;

import "./ProjectStatus.sol";
import "../rbac/RbacSmartRaise.sol";

contract Phases is ProjectStatus, RbacSmartRaise {
  uint public constant MAXPHASES = 5;
  uint public constant MINGOAL = 25;

  Phase[] public phases;

  struct Phase {
    bytes32 label;
    uint goal;
    uint dueDate;
    uint accumulatedGoal;
  }

  uint public activePhase;

  constructor() public {
    activePhase = 0;
  }

  event SetActivePhase(address indexed operator, uint activePhase);

  function setActivePhase(uint _phaseNr)
    public
    onlyRole(ROLE_AUDITOR)
  {
    activePhase = _phaseNr;

    emit SetActivePhase(msg.sender, _phaseNr);
  }

  function countPhases() public view returns (uint) {
    return phases.length;
  }

  function getPhase(uint _phNr) public view returns (Phase memory) {
    return phases[_phNr];
  }

  function totalGoal() public view returns (uint) {
    if (countPhases() > 0) {
      return getPhase(phases.length - 1).accumulatedGoal;
    } else return 0;
  }

  /// Total unlocked funding based on currently active phase. Actual payout limit is
  /// set by auditor and cannot be higher than the unlocked funding.
  function unlockedFunding() public view returns (uint) {
    return getPhase(activePhase).accumulatedGoal;
  }
}
