pragma solidity ^0.5.1;

/**  This contract provides project states and a modifier 
 *   On creation, status is Draft;
 *
 *   GLOBAL VARS:
 *
 *   StatusProject: enum {Draft, Collecting, Canceled, Hold, Funded}
 */
contract ProjectStatus {
  // TODO: Make it Bytes32-based
  enum eStatusProject {Draft, Collecting, Canceled, Hold, Funded}

  eStatusProject public statusProject;

  modifier onlyProjectStatus(eStatusProject _state) {
    require(
      statusProject == _state,
      "onlyProjectStatus check failed.");
    _;
  }

  constructor() public {
    statusProject = eStatusProject.Draft;
  }
}
