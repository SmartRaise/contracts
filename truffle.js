var HDWalletProvider = require("truffle-hdwallet-provider");

console.log("Ropsten Key: " + process.env.ropstenKey)

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*", // Match any network id
      gas: 6500000
    },
    vultr: {
      host: "199.247.23.207",
      port: 8545,
      network_id: "*" // Match any network id
    },
    ropsten: {
      // must be a thunk, otherwise truffle commands may hang in CI
      provider: () => new HDWalletProvider(process.env.ropstenKey, "https://ropsten.infura.io/dec707a3b05e46f18f4f8e521c31726b"),
      network_id: '3',
      // gas: 4600000,
      gas: 8000000,
      gasPrice: 1000000000, // 1 gwei
    }
  },
  mocha: {
    enableTimeouts: false,
    // reporter: 'eth-gas-reporter',
    reporterOptions: {
      currency: 'EUR',
      gasPrice: 8
    }
  },
  compilers: {
    solc: {
      version: "0.5.1"
    },
  },
};
      // version: "native"
      // version: "/var/lib/snapd/snap/bin/solc"
      // version: "/home/zoltan/Development/SmartRaise/SmartRaiseContracts/node_modules/solc"
