const truffleAssert = require('truffle-assertions');
const Smartraise = artifacts.require("Smartraise");
const descriptionTestData = require('./testdata/descriptionData');
const BN = require('bn.js');

contract('Description', async function (accounts) {

    // 640k gas = 1kb, so we have a 10k limit
    var instance;

    before(async function () {
        instance = await Smartraise.deployed()
        console.log("Contract address: " + instance.address)
    })

    it("Write Description", async function () {
        await instance.setDescription(JSON.stringify(descriptionTestData.testData2))
        assert(await instance.countDescriptions() == 1)
    })

    it("Read Description", async function () {
        const readback = await instance.getDescription();
        assert(readback.payload == JSON.stringify(descriptionTestData.testData2));
    })
})