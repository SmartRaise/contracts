exports.testData1 = "Hi!"
exports.testData2 = {
    title: 'hi there',
    images: ['', '', ''],
    aboutUs: 'We are really cool and nice',
    aboutProject:

        `Ethereum
From Wikipedia, the free encyclopedia

Ethereum is an open-source, public, blockchain-based distributed computing platform and operating system featuring smart contract (scripting) functionality. It supports a modified version of Nakamoto consensus via transaction-based state transitions.

Ether is a cryptocurrency whose blockchain is generated by the Ethereum platform. Ether can be transferred between accounts and used to compensate participant mining nodes for computations performed.[3] Ethereum provides a decentralized virtual machine, the Ethereum Virtual Machine (EVM), which can execute scripts using an international network of public nodes. The virtual machine's instruction set, in contrast to others like Bitcoin Script, is thought to be Turing-complete. "Gas", an internal transaction pricing mechanism, is used to mitigate spam and allocate resources on the network.

Ethereum was proposed in late 2013 by Vitalik Buterin, a cryptocurrency researcher and programmer. Development was funded by an online crowdsale that took place between July and August 2014.

The system went live on 30 July 2015, with 72 million coins "premined". This accounts for about 70 percent of the total circulating supply in 2018.

In 2016, as a result of the exploitation of a flaw in The DAO project's smart contract software, and subsequent theft of $50 million worth of Ether,[4] Ethereum was split into two separate blockchains – the new separate version became Ethereum (ETH) with the theft reversed,[5] and the original continued as Ethereum Classic (ETC).[6][7]

The value of the Ethereum currency grew over 13,000 percent in 2017, to over $1400.[8] By September 2018, it had fallen back to $200.[9]
Contents

    1 Etymology
    2 History
        2.1 Enterprise Ethereum Alliance (EEA)
        2.2 Milestones
        2.3 The DAO event
    3 Ether
        3.1 Characteristics
            3.1.1 Addresses
            3.1.2 Comparison to bitcoin
        3.2 Supply
        3.3 Markets and stores
    4 Platform
        4.1 Virtual Machine
        4.2 Smart contracts
        4.3 Applications
            4.3.1 Enterprise software
            4.3.2 Permissioned ledgers
        4.4 Performance
    5 Development Governance and EIP
    6 Criticisms
    7 References
    8 External links

Etymology

Vitalik Buterin picked the name Ethereum after browsing Wikipedia articles about elements and science fiction, when he found the name, noting, "I immediately realized that I liked it better than all of the other alternatives that I had seen; I suppose it was the fact that sounded nice and it had the word 'ether', referring to the hypothetical invisible medium that permeates the universe and allows light to travel."
History

Ethereum was initially described in a white paper by Vitalik Buterin,[10] a programmer involved with Bitcoin Magazine, in late 2013 with a goal of building decentralized applications.[11][12] Buterin had argued that Bitcoin needed a scripting language for application development. Failing to gain agreement, he proposed development of a new platform with a more general scripting language.[13]:88

At the time of public announcement in January 2014, the core Ethereum team was Vitalik Buterin, Mihai Alisie, Anthony Di Iorio, and Charles Hoskinson. Formal development of the Ethereum software project began in early 2014 through a Swiss company, Ethereum Switzerland GmbH (EthSuisse).[14][15] Subsequently, a Swiss non-profit foundation, the Ethereum Foundation (Stiftung Ethereum), was created as well. Development was funded by an online public crowdsale during July–August 2014, with the participants buying the Ethereum value token (ether) with another digital currency, bitcoin.

While there was early praise for the technical innovations of Ethereum, questions were also raised about its security and scalability.[11]
Enterprise Ethereum Alliance (EEA)

In March 2017, various blockchain start-ups, research groups, and Fortune 500 companies announced the creation of the Enterprise Ethereum Alliance (EEA) with 30 founding members.[16] By May, the nonprofit organization had 116 enterprise members—including ConsenSys, CME Group, Cornell University's research group, Toyota Research Institute, Samsung SDS, Microsoft, Intel, J. P. Morgan, Cooley LLP, Merck KGaA, DTCC, Deloitte, Accenture, Banco Santander, BNY Mellon, ING, and National Bank of Canada.[17][18][19] By July 2017, there were over 150 members in the alliance, including recent additions MasterCard, Cisco Systems, Sberbank and Scotiabank.[20][21]
Milestones
Version 	Code name 	Release date
0 	Olympic 	May, 2015
1 	Frontier 	30 July 2015
2 	Homestead 	14 March 2016
3 	Metropolis (vByzantium) 	16 October 2017
3.5 	Metropolis (vConstantinople) 	TBA
4 	Serenity 	TBA
Old version
Latest version
Future release

Several codenamed prototypes of the Ethereum platform were developed by the Foundation, as part of their Proof-of-Concept series, prior to the official launch of the Frontier network. "Olympic" was the last of these prototypes, and public beta pre-release.

The Olympic network provided users with a bug bounty of 25,000 ether for stress testing the limits of the Ethereum blockchain. "Frontier" marked the tentative experimental release of the Ethereum platform in July 2015.[22]

Since the initial launch, Ethereum has undergone several planned protocol upgrades, which are important changes affecting the underlying functionality and/or incentive structures of the platform.

[23][24]

Protocol upgrades are accomplished by means of a soft fork of the open source code base.

"Homestead" was the first to be considered stable.

It included improvements to transaction processing, gas pricing, and security and the soft fork occurred on 31 July 2015.[13]:87

The "Metropolis Part 1: Byzantium" soft fork took effect on 16 October 2017, and included changes to reduce the complexity of the EVM and provide more flexibility for smart contract developers.[24] Byzantium also adds supports for zk-SNARKs (from Zcash); with the first zk-SNARK transaction occurring on testnet on September 19, 2017.
The DAO event
Main article: The DAO (organization)

In 2016 a decentralized autonomous organization called The DAO, a set of smart contracts developed on the platform, raised a record US$150 million in a crowdsale to fund the project.[25] The DAO was exploited in June when US$50 million in Ether were taken by an unknown hacker.[26][27] The event sparked a debate in the crypto-community about whether Ethereum should perform a contentious "hard fork" to reappropriate the affected funds.[28] As a result of the dispute, the network split in two. Ethereum (the subject of this article) continued on the forked blockchain, while Ethereum Classic continued on the original blockchain.[29] The hard fork created a rivalry between the two networks.

After the hard fork related to The DAO, Ethereum subsequently forked twice in the fourth quarter of 2016 to deal with other attacks. By the end of November 2016, Ethereum had increased its DDoS protection, de-bloated the blockchain, and thwarted further spam attacks by hackers.
Ether
Ether
Denominations
Symbol	Ξ
Ticker symbol	ETH
Subunits	
 10−9	Gwei
 10−18	Wei, after cryptocurrency pionneer Wei Dai
Coins	Balances from accounts to be debited/credited, in Wei, non-UTXO scheme
Ledger
Timestamping scheme	Proof-of-work Ethash
Hash function	Keccak
Issuance	Block and Uncle/Ommer reward
Block reward	3 ETH (non-deterministic)
`
}