// import { date } from "yup";

exports.dummyProjects =
[
    {
       id:1,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:4.33611234
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:15.5123451
          },
          {
             label:"Phaselabel 3",
             dueDate:new Date("2019-03-17"),
             goal:5
          }
       ],
       description:{
          title:"Bitte helft uns, damit wir helfen können! Unsere Obdachlosenhilfe e.V.",
          aboutProject:"Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
          aboutUs:"Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
          images:[
             "assets/images/171211-F-IP259-015.JPG",
 
          ]
       },
    },
    {
       id:2,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:20
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:6
          },
          {
             label:"Phaselabel 3",
             dueDate:new Date("2019-03-17"),
             goal:10
          },
          {
             label:"Phaselabel 4",
             dueDate:new Date("2019-03-18"),
             goal:15
          }
       ],
       description:{
          title:"Schluss mit Tierquälerei!",
          aboutProject:"Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
          aboutUs:"Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
          images:[
             "assets/images/1280px-Holstein_dairy_cows.jpg",
 
          ]
       },
    },
    {
       id:3,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:25
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:26
          }
       ],
       description:{
          title:" Hungersnot in Afrika | Aktion Deutschland Hilft",
          aboutProject:"Durch die extreme Dürre am Horn von Afrika und anhaltende Gewalt in der Region um den Tschadsee leiden Millionen Menschen unter Hunger. Unterstütze die notleidenden Menschen mit Deiner Spende!",
          aboutUs:"Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
          images:[
             "assets/images/Children_in_Somalia.JPG",
 
          ]
       },
    },
    {
       id:4,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:10
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:6
          },
          {
             label:"Phaselabel 3",
             dueDate:new Date("2019-03-17"),
             goal:10
          },
          {
             label:"Phaselabel 4",
             dueDate:new Date("2019-03-18"),
             goal:5
          },
          {
             label:"Phaselabel 5",
             dueDate:new Date("2019-03-19"),
             goal:20
          }
       ],
       description:{
          title:"Dringende Rettungsaktion: Fast 10. Schildkröten brauchen Hilfe!",
          aboutUs:"Das Treiben der Fleisch- und Milchindustrie ist schockierend. Auch das der Pharma-, der Kosmetik und das der Modeindustrie: Überall auf der Welt ist es zur millionenfachen Gewohnheit geworden: Unschuldigen Tieren ihr Leben nehmen. 1933 wurde das Tierschutzgesetz verabschiedet und immer wieder angepasst. Heute, 85 Jahre später schlachten wir jährlich 800 Millionen unschuldige Tiere ab und quälen Tiere mit tödlichen Experimenten – und das alles ist völlig unnötig. Man hört immer wieder „die Politik soll es richten“. Nachdem wir nun schon 85 Jahre darauf warten, stellt sich die Frage, wie lange noch? ",
          aboutProject:"+++Aktuelle Rettungsaktion: Tausende Schildkröten brauchen dringend Hilfe - Auf Madagaskar wurden fast 11. (!) Schildkröten beschlagnahmt. In einer solchen Situation ist schnelle, unbürokratische Hilfe gefragt. Bitte hilf mit einer Spende diese Schildkröten zu retten. Mehr zu dieser Rettungsaktion+++",
          images:[
             "assets/images/Green_turtle_swimming_over_coral_reefs_in_Kona.jpg",
 
          ]
       },
    },
    {
       id:5,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:20
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:6
          },
          {
             label:"Phaselabel 3",
             dueDate:new Date("2019-03-17"),
             goal:10
          },
          {
             label:"Phaselabel 4",
             dueDate:new Date("2019-03-18"),
             goal:15
          }
       ],
       description:{
          title:"Refugee crisis: Emergency appeal",
          aboutProject:"Millions of people have been forced to flee their homes, to run for their lives and risk the safety of their families.Oxfam urgently need to help more people in Syria, Jordan, Lebanon and closer to home in Europe, too.You can help.",
          aboutUs:"Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
          images:[
             "assets/images/kitali-refugee-camp.jpg",
 
          ]
       },
    },
    {
       id:6,
       phases:[
          {
             label:"Phaselabel 1",
             dueDate:new Date("2019-03-15"),
             goal:25
          },
          {
             label:"Phaselabel 2",
             dueDate:new Date("2019-03-16"),
             goal:6
          },
          {
             label:"Phaselabel 3",
             dueDate:new Date("2019-03-17"),
             goal:30
          }
       ],
       description:{
          title:"Kanduyi children education 2",
          aboutProject:"Lorem Ipsum is simply dummy text ",
          aboutUs:"Als die Obdachlosenhilfe e.V. organisieren wir dreimal in der Woche Hilfstouren zum Hauptplatz.Dort versorgen wir bedürftige Menschen mit einer warmen Mahlzeit, Kleidung und Dingen des alltäglichen Bedarfs. Neben dieser materiellen Hilfe wollen wir durch Gespräche und Vertrauen die Lebenssituation der Menschen nachhaltig verbessen. Manchmal hilft schon ein offenes Ohr, doch auf Wunsch hin beraten wir unsere Gäste auch und  vermitteln sie zu geeigneten Stellen. Wir treten für ein solidarisches Miteinander und die Akzeptanz aller Lebensformen ein. Durch verstärkte Öffentlichkeitsarbeit soll diese Position auch der Gesellschaft nähergebracht werden. ",
          images:[
             "assets/images/children-817365_1920.jpg",
 
          ]
       },
    }
 ]