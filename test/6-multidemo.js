// const truffleAssert = require('truffle-assertions');
const Directory = artifacts.require("Directory");
const Smartraise = artifacts.require("Smartraise");

// const web3 = require("web3");

// const truffleContract = require("truffle-contract");
// const DirectoryJSON = require('../build/contracts/Directory.json');
// const Directory = truffleContract(DirectoryJSON)

const hdc = require('./helpers/deployCampaign.js');
const dummyProjects = require('./testdata/dummyProjects');


contract('Deploy Demo', async function (accounts) {
    console.log("Getting started...")

    // var instance;
    var directoryContract;
    var campaignContracts = [];

    before(async function () {
        directoryContract = await Directory.new()
        console.log("Directory address: " + directoryContract.address)
    })

    it("Set up the contracts", async function () {

        for (const project of dummyProjects.dummyProjects) {
            const newCampaign = await hdc.deployCampaign(project, 'ether')
            await directoryContract.addCampaign(newCampaign.address)
            campaignContracts.push(newCampaign)
        }
    })

    it("Set variety of statuses", async function () {
        await campaignContracts[0].setCollecting();
        await campaignContracts[1].setCollecting();
        await campaignContracts[2].setCollecting();
        await campaignContracts[3].setCollecting();
        await campaignContracts[4].setCollecting();
        await campaignContracts[5].setCollecting();
        await campaignContracts[2].setHold();
        await campaignContracts[3].setHold();
        await campaignContracts[4].setCanceled();
        await campaignContracts[5].setCanceled();
    })
})