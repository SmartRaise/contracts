const truffleAssert = require('truffle-assertions');
const Smartraise = artifacts.require("Smartraise");
const noteTestData = require('./testdata/notesData');
const BN = require('bn.js');

contract('Payment', async function (accounts) {

    // 640k gas = 1kb, so we have a 10k limit
    var instance;

    before(async function () {
        instance = await Smartraise.deployed()
        console.log("Contract address: " + instance.address)
    })

    it("Add short note", async function () {
        await instance.addAuditorNote(noteTestData.noteShort)
        assert(await instance.countAuditorNotes() == 1)
    })

    it("Add long note", async function () {
        await instance.addAuditorNote(noteTestData.noteLong)
        assert(await instance.countAuditorNotes() == 2)
    })
})