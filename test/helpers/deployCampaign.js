const truffleAssert = require('truffle-assertions');
const Directory = artifacts.require("Directory");
const Smartraise = artifacts.require("Smartraise");
// const web3 = require("web3");

// const truffleContract = require("truffle-contract");
// const DirectoryJSON = require('../build/contracts/Directory.json');
// const Directory = truffleContract(DirectoryJSON)

// const hdc = require('./helpers/deployCampaign.js');
// const dummyProjects = require('./dummyProjects');


exports.deployCampaign = async function deployCampaign(campaignData, unit = "wei" ) {

    console.log(JSON.stringify(campaignData))

    console.log("Deploying Project...")
    const newCampaign = await Smartraise.new()
    console.log("deployed: " + newCampaign.address)

    // Set Phases
    let labelsArray = [];
    let goalsArray = [];
    let dueDateArray = [];

    campaignData.phases.forEach(el => {
        labelsArray.push(web3.utils.fromUtf8(el.label))
        goalsArray.push(web3.utils.toWei(el.goal.toString(), unit))
        dueDateArray.push(el.dueDate.getTime() / 1000)
    });

    await newCampaign.setPhases(labelsArray, goalsArray, dueDateArray)

    // Set Description
    await newCampaign.setDescription(JSON.stringify(campaignData.description))
    assert(await newCampaign.countDescriptions() == 2)

    return newCampaign;
}