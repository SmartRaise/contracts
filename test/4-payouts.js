const truffleAssert = require('truffle-assertions');
const Smartraise = artifacts.require("Smartraise");
const BN = web3.utils.BN;

// const BN
// const web3 = require("web3");

// const truffleContract = require("truffle-contract");
// const DirectoryJSON = require('../build/contracts/Directory.json');
// const Directory = truffleContract(DirectoryJSON)

const hdc = require('./helpers/deployCampaign.js');
const testData = require('./testdata/payoutProjects');

contract('Payouts', async function ([pm, auditor, supporter]) {
    console.log(JSON.stringify(testData))

    var cC;

    beforeEach(async function () {
        cC = await hdc.deployCampaign(testData.payoutProjects[0])
    })

    it("Sets auditor and pays out", async function () {

        await cC.addAuditor(auditor, { from: pm });
        await cC.setCollecting({ from: pm });
        await cC.setActivePhase(0, { from: auditor })
        await cC.addFunds({ from: supporter, value: 30 })

        await cC.setFunded({ from: pm });
        await cC.approvePayoutLimit(10, { from: auditor })

        
        var pmBalancePre = new BN(await web3.eth.getBalance(pm))
        var gasPrice = await web3.eth.getGasPrice()

        var tx = await cC.payout(10, { from: pm })

        var txCost = new BN(gasPrice).mul(new BN(tx.receipt.gasUsed))
        var pmBalancePost = new BN(await web3.eth.getBalance(pm))
        assert.equal(pmBalancePost.sub(pmBalancePre.sub(txCost)).toNumber(), 10)

    })
})


/* Tests
Auditor gibt Zahlung frei
Projekt holt sich die Zahlung

Adverse conditions
- Falsche Phase
- Hoher Betrag
- Keine AuditorNote


- Payout request without proper role
- payout request more than available
- payout request more than earned
*/