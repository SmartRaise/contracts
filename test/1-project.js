const Smartraise = artifacts.require("Smartraise");

contract('Project', async function (accounts) {

    var instance;

    before(async function () {
        instance = await Smartraise.deployed()

        console.log("Contract address: " + instance.address)
    })

    it("Should initialize with draft values", async function () {
        assert.equal(await instance.statusProject().then(x => x.toNumber()), 0, 'Status Project')
        assert.equal(await instance.statusFunding().then(x => x.toNumber()), 0, 'Status Funding')

        //ActorControlled
        // assert.equal(await instance.owner(), accounts[0], 'First owner is creator');

        //RbacSmartRaise.sol
        assert.equal(await instance.hasRole(accounts[0], "pm"), true, 'Deployer becomes PM');
        assert.equal(await instance.hasRole(accounts[0], "platform"), true, 'Deployer becomes Platform');

        //Supportable
        assert.equal(await instance.totalCollected().then(x => x.toNumber()), 0, 'Total Collected is 0');
    })

    // 0.60 // 90k
    it("Should add Phases", async function () {
        await instance.setPhases([web3.utils.fromUtf8("Phase 1"), web3.utils.fromUtf8("Phase 2")], [400, 200], [2, 4])
        let phase1 = await instance.getPhase(0);
        // console.log(phase1)

        let goal = await instance.totalGoal().then(x => x.toNumber())
        assert.equal(goal, 600)
    })

    // 0.60 // 90k
    it("Set should overwrite existing phases", async function () {
        await instance.setPhases([web3.utils.fromUtf8("Phase 1"), web3.utils.fromUtf8("Phase 2")], [400, 200], [2, 4])
        await instance.setPhases([web3.utils.fromUtf8("Phase 3"), web3.utils.fromUtf8("Phase 4"), web3.utils.fromUtf8("Phase 5")], [400, 200, 300], [2, 4, 3])
        let nrPhases = await instance.countPhases().then(x => x.toNumber())
        assert.equal(nrPhases, 3)
    })

    // 0.30
    it("Reject too small phases", async function () {
        instance = await Smartraise.deployed()

        try {
            await instance.setPhases([web3.utils.fromUtf8("Phase 1")], [0], [2])
            assert.fail('Minimum Check not working')
        } catch (err) {
            let goal = await instance.totalGoal().then(x => x.toNumber())
            assert.equal(goal, 900)
        }
    })
})