const truffleAssert = require('truffle-assertions');
const Smartraise = artifacts.require("Smartraise");

contract('Payment', async function (accounts) {

    var instance;

    before(async function () {
        instance = await Smartraise.deployed()
        console.log("Contract address: " + instance.address)

        await instance.setPhases([web3.utils.fromAscii("test")], [200], [new Date("2019-03-15").getTime() / 1000])
        // await instance.addPhase(200)
        await instance.setCollecting()

    })

    // 30c // 22039
    it("Accept payments and emit event", async function () {
        let result = await instance.addFunds({ from: accounts[0], value: 10000 })
        let balance = await web3.eth.getBalance(instance.address);
        assert.equal(balance, 10000)
        truffleAssert.eventEmitted(result, 'Payin');
    })

    // 15c
    it("Reject too small payments", async function () {
        instance = await Smartraise.deployed()

        try {
            await instance.addFunds({ from: accounts[0], value: 10 })
            assert.fail('Minimum Check not working')
        } catch (err) {
            let balance = await web3.eth.getBalance(instance.address);
            assert.equal(balance, 10000)
        }

    })

    // it("Makes Paybacks", async function () {
    //     await instance.addFunds({ from: web3.eth.accounts[0], value: 10000 })
    //     let balance = web3.eth.getBalance(instance.address).toNumber();
    //     assert.equal(balance, 10000)
    // })


})