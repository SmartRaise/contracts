import Web3 = require('web3')
import * as Smartraise from "../build/contracts/Smartraise.json"

process.on( 'unhandledRejection', ( error, promise ) => {
    console.log( 'UPR: ' + promise + ' with ' + error )
    console.log( error.stack )
} )

var web3: Web3;
main().then(() => {console.log("assssss")});

async function main() {
    web3 = connect()
    let lastBlock = await web3.eth.getBlock("latest")
    console.log("Gas Limit: " + lastBlock.gasLimit)

    let gasPrice = await web3.eth.getGasPrice()
    console.log("Gas Price: " + gasPrice)


    // await deploy()

    // deploy directory
    // deploy contracts
}

function connect() {
    // const web3 = new Web3("wss://ropsten.infura.io/dec707a3b05e46f18f4f8e521c31726b");
    const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/dec707a3b05e46f18f4f8e521c31726b"));

    // const web3 = new Web3("ws://localhost:8545");
    // const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/dec707a3b05e46f18f4f8e521c31726b"));
    console.log("Created Web3, version: " + JSON.stringify(web3.version));

    web3.eth.defaultAccount = '0x797caeb56d4602d5a2cb8e12ea3b17e4fc3c1c05'
    return web3

}

async function deploy() {
    var newContract = await new web3.eth.Contract(Smartraise.abi)
    const transaction = await newContract.deploy({ data: Smartraise.bytecode, arguments: [] })
    await processTransaction(transaction, "Deploy Directory")
    console.log("rrrr")
}


async function processTransaction(transaction, label: String = "") {
    console.log("Starting " + label)
    // Run Transaction
    var gasEstimate;
    try {
        gasEstimate = await transaction.estimateGas({
            from: web3.eth.defaultAccount
        });

        console.log("Gas Estimate " + gasEstimate)
    }
    catch (e) {
        // This is where you end up with code -32603. Sadly, we can't check for that, because 
        // we only get a string error message from Metamask, but not the code.

        const explanation = `Error while trying to estimate gas. If you see the message Internal JSON-RPC Error, -32603,  \
        that's just a catch-all error, which could be the result of a VM revert, or a mismatch with your contracts. Most probable reasons \
        1) You tried to do something that's not allowed in the contract. (Any VM revert) 2) The contract and the artifact JSON have a mismatch, \
        3) Something is wrong with infura, metamask or your provider.`

        console.log(explanation + JSON.stringify(e))
    };


    if (gasEstimate) try {
        const res = await transaction.send({
            from: web3.eth.defaultAccount,
            gas: gasEstimate
        })
        // .on("error", newTrans.onError)
        // .on("transactionHash", newTrans.onHash)
        // .on("receipt", newTrans.onReceipt); // contains the new contract address

        // .on('confirmation', function (confirmationNumber, receipt) { console.log("ConfiNumber " + confirmationNumber + receipt) })
        // .then(function (newContractInstance) {
        //     console.log(newContractInstance.options.address) // instance with the new contract address
        // })); 
        console.log(JSON.stringify(res))
    }
        // Here we catch Metamask errors, like -jk
        catch (e) {
            console.log("Exception in processTransaction when trying to send transaction:" + JSON.stringify(e))

        }

}
